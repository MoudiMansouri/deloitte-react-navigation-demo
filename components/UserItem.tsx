import React from 'react'
import { Button, View, Text } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { withNavigation } from '@react-navigation/compat'

const UserItem = ({ navigation, item }) => {
    return (
        <View style={
            {
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%',
                padding: 10,
                borderWidth: 1,
                marginVertical: 5
            }}>
            <Text>{item?.name}</Text>
        </View>
    )
}

export default withNavigation(UserItem)
