import React from 'react'
import 'react-native-gesture-handler'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import {
    SafeAreaView,
    StyleSheet,
    Text
} from 'react-native'

import {
    Colors,
} from 'react-native/Libraries/NewAppScreen'
import ProfileStack from './ProfileStack'
import FeedStack from './FeedStack'
import HomeSVG from '../assets/img/home-alt.svg'
import ProfileSVG from '../assets/img/user.svg'


const Tab = createBottomTabNavigator()

export default () => {
    return (
        <NavigationContainer>
            <Tab.Navigator
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                        let icon

                        if (route.name === 'Feed') {
                            icon = focused!
                                ? <HomeSVG fill='#86BC25' height={28} width={28} />
                                : <HomeSVG fill='#53565A' height={24} width={24} />
                        } else if (route.name === 'Profile') {
                            icon = focused!
                                ? <ProfileSVG fill='#86BC25' height={28} width={28} />
                                : <ProfileSVG fill='#53565A' height={24} width={24} />
                        }
                        return icon
                    },
                })}
                tabBarOptions={{
                    activeTintColor: '#86BC25',
                    inactiveTintColor: '#53565A',
                }}
            >
                <Tab.Screen
                    name="Feed"
                    component={FeedStack}
                />
                <Tab.Screen name="Profile" component={ProfileStack} />
            </Tab.Navigator>
        </NavigationContainer>
    )
}