import React from 'react'

import { createDrawerNavigator } from '@react-navigation/drawer'

import UserFeedScreen from './../containers/UserFeedScreen'
import UserScreen from './../containers/UserScreen'
import Profile from '../containers/Profile'
import SettingsScreen from './../containers/SettingsScreen'
import { createStackNavigator } from '@react-navigation/stack'

const Drawer = createDrawerNavigator()
const PStack = createStackNavigator()
const SStack = createStackNavigator()
const ProfileStack = () => (
    <PStack.Navigator>
        <PStack.Screen name="Profile" component={Profile} />
    </PStack.Navigator>
)

const SettingsStack = () => (
    <PStack.Navigator>
        <SStack.Screen name="Settings" component={SettingsScreen} />
    </PStack.Navigator>
)

const ProfileDrawer = () => {
    return (
        <>
            <Drawer.Navigator initialRouteName="Profile" drawerPosition="right">
                <Drawer.Screen name="Profile" component={ProfileStack} />
                <Drawer.Screen name="Setting" component={SettingsStack} />
            </Drawer.Navigator>
        </>
    )
}

export default ProfileDrawer