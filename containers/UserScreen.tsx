import React, { useEffect } from 'react'
import { SafeAreaView, Text } from "react-native"
import { getUser } from '../redux/appReducer'
import { connect } from 'react-redux'

const UserScreen = ({ route, navigation, user }) => {
    useEffect(() => {
        navigation.setOptions({ title: user(route.params.userId).name })
    })
    return (
        <SafeAreaView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>{user(route.params.userId).name}</Text>
        </SafeAreaView>
    )
}

const mapStateToProps = (state) => {
    return {
        user: (id) => getUser(state, id)
    }
}
export default connect(mapStateToProps)(UserScreen)