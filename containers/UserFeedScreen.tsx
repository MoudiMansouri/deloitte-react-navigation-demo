import React from 'react'
import { SafeAreaView, Text, FlatList } from "react-native"
import UserItem from './../components/UserItem'
import { getUsers } from '../redux/appReducer'
import { connect } from 'react-redux'
import { TouchableOpacity } from 'react-native-gesture-handler'




const UserFeedScreen = ({ users, navigation }) => {
    return (
        <SafeAreaView style={{ flex: 1, alignItems: 'center', padding: 10 }}>
            <FlatList
                data={Object.values(users)}
                renderItem={({ item }) => (
                    <TouchableOpacity
                        onPress={() => navigation.navigate('User', { userId: item.id })}
                        style={{ width: '100%' }}
                    ><UserItem item={item} />
                    </TouchableOpacity>

                )}
                keyExtractor={item => item.id}
            />
        </SafeAreaView>
    )
}

const mapStateToProps = (state) => {
    return {
        users: getUsers(state)
    }
}
export default connect(mapStateToProps)(UserFeedScreen)