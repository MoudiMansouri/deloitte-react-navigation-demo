import { State } from "react-native-gesture-handler"

const updateName = (name: string) => {
    return {
        type: 'UPDATE_NAME',
        name
    }
}

const initialState = {
    // Imagine this is returned by a server after Authenticating and stored in redux
    loggedInUserId: "3",
    loggedInUser: {
        id: "3",
        name: "Mohamad Mansouri"
    },
    users: {
        "1": {
            id: "1",
            name: "John Doe"
        },
        "2": {
            id: "2",
            name: "Jane Doe"
        },
        "3": {
            id: "3",
            name: "Mohamad Mansouri"
        }
    }
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_NAME':
            return {
                ...state,
                users: {
                    ...state.users,
                    [state.loggedInUserId]: {
                        ...state.users[state.loggedInUserId],
                        name: action.name
                    }
                }
            }
        default:
            return state
    }
}

const getUsers = (state) => state.users
const getUser = (state, id) => state.users[id]



export { updateName, getUsers, getUser }